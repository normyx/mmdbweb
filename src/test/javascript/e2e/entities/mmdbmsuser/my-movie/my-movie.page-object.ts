import { element, by, ElementFinder } from 'protractor';

export class MyMovieComponentsPage {
  createButton = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('jhi-my-movie div table .btn-danger'));
  title = element.all(by.css('jhi-my-movie div h2#page-heading span')).first();
  noResult = element(by.id('no-result'));
  entities = element(by.id('entities'));

  async clickOnCreateButton(): Promise<void> {
    await this.createButton.click();
  }

  async clickOnLastDeleteButton(): Promise<void> {
    await this.deleteButtons.last().click();
  }

  async countDeleteButtons(): Promise<number> {
    return this.deleteButtons.count();
  }

  async getTitle(): Promise<string> {
    return this.title.getAttribute('jhiTranslate');
  }
}

export class MyMovieUpdatePage {
  pageTitle = element(by.id('jhi-my-movie-heading'));
  saveButton = element(by.id('save-entity'));
  cancelButton = element(by.id('cancel-save'));

  idInput = element(by.id('field_id'));
  commentsInput = element(by.id('field_comments'));
  voteInput = element(by.id('field_vote'));
  viewedDateInput = element(by.id('field_viewedDate'));
  tmdbIdInput = element(by.id('field_tmdbId'));
  userIdInput = element(by.id('field_userId'));

  async getPageTitle(): Promise<string> {
    return this.pageTitle.getAttribute('jhiTranslate');
  }

  async setIdInput(id: string): Promise<void> {
    await this.idInput.sendKeys(id);
  }

  async getIdInput(): Promise<string> {
    return await this.idInput.getAttribute('value');
  }

  async setCommentsInput(comments: string): Promise<void> {
    await this.commentsInput.sendKeys(comments);
  }

  async getCommentsInput(): Promise<string> {
    return await this.commentsInput.getAttribute('value');
  }

  async setVoteInput(vote: string): Promise<void> {
    await this.voteInput.sendKeys(vote);
  }

  async getVoteInput(): Promise<string> {
    return await this.voteInput.getAttribute('value');
  }

  async setViewedDateInput(viewedDate: string): Promise<void> {
    await this.viewedDateInput.sendKeys(viewedDate);
  }

  async getViewedDateInput(): Promise<string> {
    return await this.viewedDateInput.getAttribute('value');
  }

  async setTmdbIdInput(tmdbId: string): Promise<void> {
    await this.tmdbIdInput.sendKeys(tmdbId);
  }

  async getTmdbIdInput(): Promise<string> {
    return await this.tmdbIdInput.getAttribute('value');
  }

  async setUserIdInput(userId: string): Promise<void> {
    await this.userIdInput.sendKeys(userId);
  }

  async getUserIdInput(): Promise<string> {
    return await this.userIdInput.getAttribute('value');
  }

  async save(): Promise<void> {
    await this.saveButton.click();
  }

  async cancel(): Promise<void> {
    await this.cancelButton.click();
  }

  getSaveButton(): ElementFinder {
    return this.saveButton;
  }
}

export class MyMovieDeleteDialog {
  private dialogTitle = element(by.id('jhi-delete-myMovie-heading'));
  private confirmButton = element(by.id('jhi-confirm-delete-myMovie'));

  async getDialogTitle(): Promise<string> {
    return this.dialogTitle.getAttribute('jhiTranslate');
  }

  async clickOnConfirmButton(): Promise<void> {
    await this.confirmButton.click();
  }
}
