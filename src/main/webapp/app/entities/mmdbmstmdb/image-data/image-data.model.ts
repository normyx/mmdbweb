import { IImage } from 'app/entities/mmdbmstmdb/image/image.model';

export interface IImageData {
  id?: number;
  imageSize?: string;
  imageBytesContentType?: string;
  imageBytes?: string;
  image?: IImage;
}

export class ImageData implements IImageData {
  constructor(
    public id?: number,
    public imageSize?: string,
    public imageBytesContentType?: string,
    public imageBytes?: string,
    public image?: IImage
  ) {}
}

export function getImageDataIdentifier(imageData: IImageData): number | undefined {
  return imageData.id;
}
