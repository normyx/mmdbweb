import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { ImageDataComponent } from './list/image-data.component';
import { ImageDataDetailComponent } from './detail/image-data-detail.component';
import { ImageDataUpdateComponent } from './update/image-data-update.component';
import { ImageDataDeleteDialogComponent } from './delete/image-data-delete-dialog.component';
import { ImageDataRoutingModule } from './route/image-data-routing.module';

@NgModule({
  imports: [SharedModule, ImageDataRoutingModule],
  declarations: [ImageDataComponent, ImageDataDetailComponent, ImageDataUpdateComponent, ImageDataDeleteDialogComponent],
  entryComponents: [ImageDataDeleteDialogComponent],
})
export class MmdbmstmdbImageDataModule {}
