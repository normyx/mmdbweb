import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IImageData, ImageData } from '../image-data.model';
import { ImageDataService } from '../service/image-data.service';

@Injectable({ providedIn: 'root' })
export class ImageDataRoutingResolveService implements Resolve<IImageData> {
  constructor(protected service: ImageDataService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IImageData> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((imageData: HttpResponse<ImageData>) => {
          if (imageData.body) {
            return of(imageData.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ImageData());
  }
}
