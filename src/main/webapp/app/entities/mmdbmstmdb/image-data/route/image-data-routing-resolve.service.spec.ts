jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IImageData, ImageData } from '../image-data.model';
import { ImageDataService } from '../service/image-data.service';

import { ImageDataRoutingResolveService } from './image-data-routing-resolve.service';

describe('Service Tests', () => {
  describe('ImageData routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: ImageDataRoutingResolveService;
    let service: ImageDataService;
    let resultImageData: IImageData | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(ImageDataRoutingResolveService);
      service = TestBed.inject(ImageDataService);
      resultImageData = undefined;
    });

    describe('resolve', () => {
      it('should return IImageData returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultImageData = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultImageData).toEqual({ id: 123 });
      });

      it('should return new IImageData if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultImageData = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultImageData).toEqual(new ImageData());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultImageData = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultImageData).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
