import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ImageDataComponent } from '../list/image-data.component';
import { ImageDataDetailComponent } from '../detail/image-data-detail.component';
import { ImageDataUpdateComponent } from '../update/image-data-update.component';
import { ImageDataRoutingResolveService } from './image-data-routing-resolve.service';

const imageDataRoute: Routes = [
  {
    path: '',
    component: ImageDataComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ImageDataDetailComponent,
    resolve: {
      imageData: ImageDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ImageDataUpdateComponent,
    resolve: {
      imageData: ImageDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ImageDataUpdateComponent,
    resolve: {
      imageData: ImageDataRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(imageDataRoute)],
  exports: [RouterModule],
})
export class ImageDataRoutingModule {}
