import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IImageData } from '../image-data.model';
import { ImageDataService } from '../service/image-data.service';

@Component({
  templateUrl: './image-data-delete-dialog.component.html',
})
export class ImageDataDeleteDialogComponent {
  imageData?: IImageData;

  constructor(protected imageDataService: ImageDataService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.imageDataService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
