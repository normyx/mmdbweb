import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IImageData, getImageDataIdentifier } from '../image-data.model';

export type EntityResponseType = HttpResponse<IImageData>;
export type EntityArrayResponseType = HttpResponse<IImageData[]>;

@Injectable({ providedIn: 'root' })
export class ImageDataService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/image-data', 'mmdbmstmdb');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(imageData: IImageData): Observable<EntityResponseType> {
    return this.http.post<IImageData>(this.resourceUrl, imageData, { observe: 'response' });
  }

  update(imageData: IImageData): Observable<EntityResponseType> {
    return this.http.put<IImageData>(`${this.resourceUrl}/${getImageDataIdentifier(imageData) as number}`, imageData, {
      observe: 'response',
    });
  }

  partialUpdate(imageData: IImageData): Observable<EntityResponseType> {
    return this.http.patch<IImageData>(`${this.resourceUrl}/${getImageDataIdentifier(imageData) as number}`, imageData, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IImageData>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IImageData[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addImageDataToCollectionIfMissing(
    imageDataCollection: IImageData[],
    ...imageDataToCheck: (IImageData | null | undefined)[]
  ): IImageData[] {
    const imageData: IImageData[] = imageDataToCheck.filter(isPresent);
    if (imageData.length > 0) {
      const imageDataCollectionIdentifiers = imageDataCollection.map(imageDataItem => getImageDataIdentifier(imageDataItem)!);
      const imageDataToAdd = imageData.filter(imageDataItem => {
        const imageDataIdentifier = getImageDataIdentifier(imageDataItem);
        if (imageDataIdentifier == null || imageDataCollectionIdentifiers.includes(imageDataIdentifier)) {
          return false;
        }
        imageDataCollectionIdentifiers.push(imageDataIdentifier);
        return true;
      });
      return [...imageDataToAdd, ...imageDataCollection];
    }
    return imageDataCollection;
  }
}
