import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IImageData, ImageData } from '../image-data.model';

import { ImageDataService } from './image-data.service';

describe('Service Tests', () => {
  describe('ImageData Service', () => {
    let service: ImageDataService;
    let httpMock: HttpTestingController;
    let elemDefault: IImageData;
    let expectedResult: IImageData | IImageData[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(ImageDataService);
      httpMock = TestBed.inject(HttpTestingController);

      elemDefault = {
        id: 0,
        imageSize: 'AAAAAAA',
        imageBytesContentType: 'image/png',
        imageBytes: 'AAAAAAA',
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a ImageData', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new ImageData()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a ImageData', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            imageSize: 'BBBBBB',
            imageBytes: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a ImageData', () => {
        const patchObject = Object.assign({}, new ImageData());

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign({}, returnedFromService);

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of ImageData', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            imageSize: 'BBBBBB',
            imageBytes: 'BBBBBB',
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a ImageData', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addImageDataToCollectionIfMissing', () => {
        it('should add a ImageData to an empty array', () => {
          const imageData: IImageData = { id: 123 };
          expectedResult = service.addImageDataToCollectionIfMissing([], imageData);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(imageData);
        });

        it('should not add a ImageData to an array that contains it', () => {
          const imageData: IImageData = { id: 123 };
          const imageDataCollection: IImageData[] = [
            {
              ...imageData,
            },
            { id: 456 },
          ];
          expectedResult = service.addImageDataToCollectionIfMissing(imageDataCollection, imageData);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a ImageData to an array that doesn't contain it", () => {
          const imageData: IImageData = { id: 123 };
          const imageDataCollection: IImageData[] = [{ id: 456 }];
          expectedResult = service.addImageDataToCollectionIfMissing(imageDataCollection, imageData);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(imageData);
        });

        it('should add only unique ImageData to an array', () => {
          const imageDataArray: IImageData[] = [{ id: 123 }, { id: 456 }, { id: 65373 }];
          const imageDataCollection: IImageData[] = [{ id: 123 }];
          expectedResult = service.addImageDataToCollectionIfMissing(imageDataCollection, ...imageDataArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const imageData: IImageData = { id: 123 };
          const imageData2: IImageData = { id: 456 };
          expectedResult = service.addImageDataToCollectionIfMissing([], imageData, imageData2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(imageData);
          expect(expectedResult).toContain(imageData2);
        });

        it('should accept null and undefined values', () => {
          const imageData: IImageData = { id: 123 };
          expectedResult = service.addImageDataToCollectionIfMissing([], null, imageData, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(imageData);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
