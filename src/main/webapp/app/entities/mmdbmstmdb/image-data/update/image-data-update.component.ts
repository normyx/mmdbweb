import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IImageData, ImageData } from '../image-data.model';
import { ImageDataService } from '../service/image-data.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { IImage } from 'app/entities/mmdbmstmdb/image/image.model';
import { ImageService } from 'app/entities/mmdbmstmdb/image/service/image.service';

@Component({
  selector: 'jhi-image-data-update',
  templateUrl: './image-data-update.component.html',
})
export class ImageDataUpdateComponent implements OnInit {
  isSaving = false;

  imagesSharedCollection: IImage[] = [];

  editForm = this.fb.group({
    id: [],
    imageSize: [null, [Validators.required, Validators.maxLength(40)]],
    imageBytes: [null, [Validators.required]],
    imageBytesContentType: [],
    image: [null, Validators.required],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected imageDataService: ImageDataService,
    protected imageService: ImageService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ imageData }) => {
      this.updateForm(imageData);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(
          new EventWithContent<AlertError>('mmdbwebApp.error', { ...err, key: 'error.file.' + err.key })
        ),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const imageData = this.createFromForm();
    if (imageData.id !== undefined) {
      this.subscribeToSaveResponse(this.imageDataService.update(imageData));
    } else {
      this.subscribeToSaveResponse(this.imageDataService.create(imageData));
    }
  }

  trackImageById(index: number, item: IImage): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImageData>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(imageData: IImageData): void {
    this.editForm.patchValue({
      id: imageData.id,
      imageSize: imageData.imageSize,
      imageBytes: imageData.imageBytes,
      imageBytesContentType: imageData.imageBytesContentType,
      image: imageData.image,
    });

    this.imagesSharedCollection = this.imageService.addImageToCollectionIfMissing(this.imagesSharedCollection, imageData.image);
  }

  protected loadRelationshipsOptions(): void {
    this.imageService
      .query()
      .pipe(map((res: HttpResponse<IImage[]>) => res.body ?? []))
      .pipe(map((images: IImage[]) => this.imageService.addImageToCollectionIfMissing(images, this.editForm.get('image')!.value)))
      .subscribe((images: IImage[]) => (this.imagesSharedCollection = images));
  }

  protected createFromForm(): IImageData {
    return {
      ...new ImageData(),
      id: this.editForm.get(['id'])!.value,
      imageSize: this.editForm.get(['imageSize'])!.value,
      imageBytesContentType: this.editForm.get(['imageBytesContentType'])!.value,
      imageBytes: this.editForm.get(['imageBytes'])!.value,
      image: this.editForm.get(['image'])!.value,
    };
  }
}
