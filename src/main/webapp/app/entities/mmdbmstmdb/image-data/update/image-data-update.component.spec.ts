jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ImageDataService } from '../service/image-data.service';
import { IImageData, ImageData } from '../image-data.model';
import { IImage } from 'app/entities/mmdbmstmdb/image/image.model';
import { ImageService } from 'app/entities/mmdbmstmdb/image/service/image.service';

import { ImageDataUpdateComponent } from './image-data-update.component';

describe('Component Tests', () => {
  describe('ImageData Management Update Component', () => {
    let comp: ImageDataUpdateComponent;
    let fixture: ComponentFixture<ImageDataUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let imageDataService: ImageDataService;
    let imageService: ImageService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ImageDataUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ImageDataUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ImageDataUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      imageDataService = TestBed.inject(ImageDataService);
      imageService = TestBed.inject(ImageService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Image query and add missing value', () => {
        const imageData: IImageData = { id: 456 };
        const image: IImage = { id: 82531 };
        imageData.image = image;

        const imageCollection: IImage[] = [{ id: 88006 }];
        spyOn(imageService, 'query').and.returnValue(of(new HttpResponse({ body: imageCollection })));
        const additionalImages = [image];
        const expectedCollection: IImage[] = [...additionalImages, ...imageCollection];
        spyOn(imageService, 'addImageToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ imageData });
        comp.ngOnInit();

        expect(imageService.query).toHaveBeenCalled();
        expect(imageService.addImageToCollectionIfMissing).toHaveBeenCalledWith(imageCollection, ...additionalImages);
        expect(comp.imagesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const imageData: IImageData = { id: 456 };
        const image: IImage = { id: 88305 };
        imageData.image = image;

        activatedRoute.data = of({ imageData });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(imageData));
        expect(comp.imagesSharedCollection).toContain(image);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const imageData = { id: 123 };
        spyOn(imageDataService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ imageData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: imageData }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(imageDataService.update).toHaveBeenCalledWith(imageData);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const imageData = new ImageData();
        spyOn(imageDataService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ imageData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: imageData }));
        saveSubject.complete();

        // THEN
        expect(imageDataService.create).toHaveBeenCalledWith(imageData);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const imageData = { id: 123 };
        spyOn(imageDataService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ imageData });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(imageDataService.update).toHaveBeenCalledWith(imageData);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackImageById', () => {
        it('Should return tracked Image primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackImageById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
