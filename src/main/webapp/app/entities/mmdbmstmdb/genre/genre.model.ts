import * as dayjs from 'dayjs';
import { IMovie } from 'app/entities/mmdbmstmdb/movie/movie.model';

export interface IGenre {
  id?: number;
  tmdbId?: string;
  name?: string;
  lastTMDBUpdate?: dayjs.Dayjs;
  movies?: IMovie[] | null;
}

export class Genre implements IGenre {
  constructor(
    public id?: number,
    public tmdbId?: string,
    public name?: string,
    public lastTMDBUpdate?: dayjs.Dayjs,
    public movies?: IMovie[] | null
  ) {}
}

export function getGenreIdentifier(genre: IGenre): number | undefined {
  return genre.id;
}
