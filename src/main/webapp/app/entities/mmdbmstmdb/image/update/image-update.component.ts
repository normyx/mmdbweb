import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IImage, Image } from '../image.model';
import { ImageService } from '../service/image.service';
import { IMovie } from 'app/entities/mmdbmstmdb/movie/movie.model';
import { MovieService } from 'app/entities/mmdbmstmdb/movie/service/movie.service';
import { IPerson } from 'app/entities/mmdbmstmdb/person/person.model';
import { PersonService } from 'app/entities/mmdbmstmdb/person/service/person.service';

@Component({
  selector: 'jhi-image-update',
  templateUrl: './image-update.component.html',
})
export class ImageUpdateComponent implements OnInit {
  isSaving = false;

  moviesSharedCollection: IMovie[] = [];
  peopleSharedCollection: IPerson[] = [];

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    locale: [],
    voteAverage: [],
    voteCount: [],
    lastTMDBUpdate: [null, [Validators.required]],
    posterMovie: [],
    backdropMovie: [],
    person: [],
  });

  constructor(
    protected imageService: ImageService,
    protected movieService: MovieService,
    protected personService: PersonService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ image }) => {
      this.updateForm(image);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const image = this.createFromForm();
    if (image.id !== undefined) {
      this.subscribeToSaveResponse(this.imageService.update(image));
    } else {
      this.subscribeToSaveResponse(this.imageService.create(image));
    }
  }

  trackMovieById(index: number, item: IMovie): number {
    return item.id!;
  }

  trackPersonById(index: number, item: IPerson): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IImage>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(image: IImage): void {
    this.editForm.patchValue({
      id: image.id,
      tmdbId: image.tmdbId,
      locale: image.locale,
      voteAverage: image.voteAverage,
      voteCount: image.voteCount,
      lastTMDBUpdate: image.lastTMDBUpdate,
      posterMovie: image.posterMovie,
      backdropMovie: image.backdropMovie,
      person: image.person,
    });

    this.moviesSharedCollection = this.movieService.addMovieToCollectionIfMissing(
      this.moviesSharedCollection,
      image.posterMovie,
      image.backdropMovie
    );
    this.peopleSharedCollection = this.personService.addPersonToCollectionIfMissing(this.peopleSharedCollection, image.person);
  }

  protected loadRelationshipsOptions(): void {
    this.movieService
      .query()
      .pipe(map((res: HttpResponse<IMovie[]>) => res.body ?? []))
      .pipe(
        map((movies: IMovie[]) =>
          this.movieService.addMovieToCollectionIfMissing(
            movies,
            this.editForm.get('posterMovie')!.value,
            this.editForm.get('backdropMovie')!.value
          )
        )
      )
      .subscribe((movies: IMovie[]) => (this.moviesSharedCollection = movies));

    this.personService
      .query()
      .pipe(map((res: HttpResponse<IPerson[]>) => res.body ?? []))
      .pipe(map((people: IPerson[]) => this.personService.addPersonToCollectionIfMissing(people, this.editForm.get('person')!.value)))
      .subscribe((people: IPerson[]) => (this.peopleSharedCollection = people));
  }

  protected createFromForm(): IImage {
    return {
      ...new Image(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      locale: this.editForm.get(['locale'])!.value,
      voteAverage: this.editForm.get(['voteAverage'])!.value,
      voteCount: this.editForm.get(['voteCount'])!.value,
      lastTMDBUpdate: this.editForm.get(['lastTMDBUpdate'])!.value,
      posterMovie: this.editForm.get(['posterMovie'])!.value,
      backdropMovie: this.editForm.get(['backdropMovie'])!.value,
      person: this.editForm.get(['person'])!.value,
    };
  }
}
