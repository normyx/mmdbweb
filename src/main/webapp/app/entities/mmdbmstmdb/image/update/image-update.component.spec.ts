jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { ImageService } from '../service/image.service';
import { IImage, Image } from '../image.model';
import { IMovie } from 'app/entities/mmdbmstmdb/movie/movie.model';
import { MovieService } from 'app/entities/mmdbmstmdb/movie/service/movie.service';
import { IPerson } from 'app/entities/mmdbmstmdb/person/person.model';
import { PersonService } from 'app/entities/mmdbmstmdb/person/service/person.service';

import { ImageUpdateComponent } from './image-update.component';

describe('Component Tests', () => {
  describe('Image Management Update Component', () => {
    let comp: ImageUpdateComponent;
    let fixture: ComponentFixture<ImageUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let imageService: ImageService;
    let movieService: MovieService;
    let personService: PersonService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [ImageUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(ImageUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ImageUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      imageService = TestBed.inject(ImageService);
      movieService = TestBed.inject(MovieService);
      personService = TestBed.inject(PersonService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Movie query and add missing value', () => {
        const image: IImage = { id: 456 };
        const posterMovie: IMovie = { id: 1695 };
        image.posterMovie = posterMovie;
        const backdropMovie: IMovie = { id: 70293 };
        image.backdropMovie = backdropMovie;

        const movieCollection: IMovie[] = [{ id: 68738 }];
        spyOn(movieService, 'query').and.returnValue(of(new HttpResponse({ body: movieCollection })));
        const additionalMovies = [posterMovie, backdropMovie];
        const expectedCollection: IMovie[] = [...additionalMovies, ...movieCollection];
        spyOn(movieService, 'addMovieToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ image });
        comp.ngOnInit();

        expect(movieService.query).toHaveBeenCalled();
        expect(movieService.addMovieToCollectionIfMissing).toHaveBeenCalledWith(movieCollection, ...additionalMovies);
        expect(comp.moviesSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Person query and add missing value', () => {
        const image: IImage = { id: 456 };
        const person: IPerson = { id: 72129 };
        image.person = person;

        const personCollection: IPerson[] = [{ id: 82230 }];
        spyOn(personService, 'query').and.returnValue(of(new HttpResponse({ body: personCollection })));
        const additionalPeople = [person];
        const expectedCollection: IPerson[] = [...additionalPeople, ...personCollection];
        spyOn(personService, 'addPersonToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ image });
        comp.ngOnInit();

        expect(personService.query).toHaveBeenCalled();
        expect(personService.addPersonToCollectionIfMissing).toHaveBeenCalledWith(personCollection, ...additionalPeople);
        expect(comp.peopleSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const image: IImage = { id: 456 };
        const posterMovie: IMovie = { id: 46499 };
        image.posterMovie = posterMovie;
        const backdropMovie: IMovie = { id: 78307 };
        image.backdropMovie = backdropMovie;
        const person: IPerson = { id: 15931 };
        image.person = person;

        activatedRoute.data = of({ image });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(image));
        expect(comp.moviesSharedCollection).toContain(posterMovie);
        expect(comp.moviesSharedCollection).toContain(backdropMovie);
        expect(comp.peopleSharedCollection).toContain(person);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const image = { id: 123 };
        spyOn(imageService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ image });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: image }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(imageService.update).toHaveBeenCalledWith(image);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const image = new Image();
        spyOn(imageService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ image });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: image }));
        saveSubject.complete();

        // THEN
        expect(imageService.create).toHaveBeenCalledWith(image);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const image = { id: 123 };
        spyOn(imageService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ image });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(imageService.update).toHaveBeenCalledWith(image);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackMovieById', () => {
        it('Should return tracked Movie primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMovieById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackPersonById', () => {
        it('Should return tracked Person primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackPersonById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
