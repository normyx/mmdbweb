import * as dayjs from 'dayjs';
import { IMovie } from 'app/entities/mmdbmstmdb/movie/movie.model';
import { IPerson } from 'app/entities/mmdbmstmdb/person/person.model';

export interface IImage {
  id?: number;
  tmdbId?: string;
  locale?: string | null;
  voteAverage?: number | null;
  voteCount?: number | null;
  lastTMDBUpdate?: dayjs.Dayjs;
  posterMovie?: IMovie | null;
  backdropMovie?: IMovie | null;
  person?: IPerson | null;
}

export class Image implements IImage {
  constructor(
    public id?: number,
    public tmdbId?: string,
    public locale?: string | null,
    public voteAverage?: number | null,
    public voteCount?: number | null,
    public lastTMDBUpdate?: dayjs.Dayjs,
    public posterMovie?: IMovie | null,
    public backdropMovie?: IMovie | null,
    public person?: IPerson | null
  ) {}
}

export function getImageIdentifier(image: IImage): number | undefined {
  return image.id;
}
