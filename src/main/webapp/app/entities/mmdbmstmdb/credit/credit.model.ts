import * as dayjs from 'dayjs';
import { IPerson } from 'app/entities/mmdbmstmdb/person/person.model';
import { IMovie } from 'app/entities/mmdbmstmdb/movie/movie.model';

export interface ICredit {
  id?: number;
  tmdbId?: string;
  character?: string | null;
  creditType?: string | null;
  department?: string | null;
  job?: string | null;
  order?: number | null;
  lastTMDBUpdate?: dayjs.Dayjs;
  person?: IPerson | null;
  movie?: IMovie | null;
}

export class Credit implements ICredit {
  constructor(
    public id?: number,
    public tmdbId?: string,
    public character?: string | null,
    public creditType?: string | null,
    public department?: string | null,
    public job?: string | null,
    public order?: number | null,
    public lastTMDBUpdate?: dayjs.Dayjs,
    public person?: IPerson | null,
    public movie?: IMovie | null
  ) {}
}

export function getCreditIdentifier(credit: ICredit): number | undefined {
  return credit.id;
}
