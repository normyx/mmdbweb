import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ICredit, Credit } from '../credit.model';
import { CreditService } from '../service/credit.service';
import { IPerson } from 'app/entities/mmdbmstmdb/person/person.model';
import { PersonService } from 'app/entities/mmdbmstmdb/person/service/person.service';
import { IMovie } from 'app/entities/mmdbmstmdb/movie/movie.model';
import { MovieService } from 'app/entities/mmdbmstmdb/movie/service/movie.service';

@Component({
  selector: 'jhi-credit-update',
  templateUrl: './credit-update.component.html',
})
export class CreditUpdateComponent implements OnInit {
  isSaving = false;

  peopleSharedCollection: IPerson[] = [];
  moviesSharedCollection: IMovie[] = [];

  editForm = this.fb.group({
    id: [],
    tmdbId: [null, [Validators.required]],
    character: [null, [Validators.maxLength(200)]],
    creditType: [],
    department: [],
    job: [],
    order: [],
    lastTMDBUpdate: [null, [Validators.required]],
    person: [],
    movie: [],
  });

  constructor(
    protected creditService: CreditService,
    protected personService: PersonService,
    protected movieService: MovieService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ credit }) => {
      this.updateForm(credit);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const credit = this.createFromForm();
    if (credit.id !== undefined) {
      this.subscribeToSaveResponse(this.creditService.update(credit));
    } else {
      this.subscribeToSaveResponse(this.creditService.create(credit));
    }
  }

  trackPersonById(index: number, item: IPerson): number {
    return item.id!;
  }

  trackMovieById(index: number, item: IMovie): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICredit>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(credit: ICredit): void {
    this.editForm.patchValue({
      id: credit.id,
      tmdbId: credit.tmdbId,
      character: credit.character,
      creditType: credit.creditType,
      department: credit.department,
      job: credit.job,
      order: credit.order,
      lastTMDBUpdate: credit.lastTMDBUpdate,
      person: credit.person,
      movie: credit.movie,
    });

    this.peopleSharedCollection = this.personService.addPersonToCollectionIfMissing(this.peopleSharedCollection, credit.person);
    this.moviesSharedCollection = this.movieService.addMovieToCollectionIfMissing(this.moviesSharedCollection, credit.movie);
  }

  protected loadRelationshipsOptions(): void {
    this.personService
      .query()
      .pipe(map((res: HttpResponse<IPerson[]>) => res.body ?? []))
      .pipe(map((people: IPerson[]) => this.personService.addPersonToCollectionIfMissing(people, this.editForm.get('person')!.value)))
      .subscribe((people: IPerson[]) => (this.peopleSharedCollection = people));

    this.movieService
      .query()
      .pipe(map((res: HttpResponse<IMovie[]>) => res.body ?? []))
      .pipe(map((movies: IMovie[]) => this.movieService.addMovieToCollectionIfMissing(movies, this.editForm.get('movie')!.value)))
      .subscribe((movies: IMovie[]) => (this.moviesSharedCollection = movies));
  }

  protected createFromForm(): ICredit {
    return {
      ...new Credit(),
      id: this.editForm.get(['id'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      character: this.editForm.get(['character'])!.value,
      creditType: this.editForm.get(['creditType'])!.value,
      department: this.editForm.get(['department'])!.value,
      job: this.editForm.get(['job'])!.value,
      order: this.editForm.get(['order'])!.value,
      lastTMDBUpdate: this.editForm.get(['lastTMDBUpdate'])!.value,
      person: this.editForm.get(['person'])!.value,
      movie: this.editForm.get(['movie'])!.value,
    };
  }
}
