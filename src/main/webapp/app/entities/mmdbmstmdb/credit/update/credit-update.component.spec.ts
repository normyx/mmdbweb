jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { CreditService } from '../service/credit.service';
import { ICredit, Credit } from '../credit.model';
import { IPerson } from 'app/entities/mmdbmstmdb/person/person.model';
import { PersonService } from 'app/entities/mmdbmstmdb/person/service/person.service';
import { IMovie } from 'app/entities/mmdbmstmdb/movie/movie.model';
import { MovieService } from 'app/entities/mmdbmstmdb/movie/service/movie.service';

import { CreditUpdateComponent } from './credit-update.component';

describe('Component Tests', () => {
  describe('Credit Management Update Component', () => {
    let comp: CreditUpdateComponent;
    let fixture: ComponentFixture<CreditUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let creditService: CreditService;
    let personService: PersonService;
    let movieService: MovieService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [CreditUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(CreditUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CreditUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      creditService = TestBed.inject(CreditService);
      personService = TestBed.inject(PersonService);
      movieService = TestBed.inject(MovieService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Person query and add missing value', () => {
        const credit: ICredit = { id: 456 };
        const person: IPerson = { id: 10973 };
        credit.person = person;

        const personCollection: IPerson[] = [{ id: 3003 }];
        spyOn(personService, 'query').and.returnValue(of(new HttpResponse({ body: personCollection })));
        const additionalPeople = [person];
        const expectedCollection: IPerson[] = [...additionalPeople, ...personCollection];
        spyOn(personService, 'addPersonToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ credit });
        comp.ngOnInit();

        expect(personService.query).toHaveBeenCalled();
        expect(personService.addPersonToCollectionIfMissing).toHaveBeenCalledWith(personCollection, ...additionalPeople);
        expect(comp.peopleSharedCollection).toEqual(expectedCollection);
      });

      it('Should call Movie query and add missing value', () => {
        const credit: ICredit = { id: 456 };
        const movie: IMovie = { id: 21395 };
        credit.movie = movie;

        const movieCollection: IMovie[] = [{ id: 78651 }];
        spyOn(movieService, 'query').and.returnValue(of(new HttpResponse({ body: movieCollection })));
        const additionalMovies = [movie];
        const expectedCollection: IMovie[] = [...additionalMovies, ...movieCollection];
        spyOn(movieService, 'addMovieToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ credit });
        comp.ngOnInit();

        expect(movieService.query).toHaveBeenCalled();
        expect(movieService.addMovieToCollectionIfMissing).toHaveBeenCalledWith(movieCollection, ...additionalMovies);
        expect(comp.moviesSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const credit: ICredit = { id: 456 };
        const person: IPerson = { id: 35528 };
        credit.person = person;
        const movie: IMovie = { id: 54806 };
        credit.movie = movie;

        activatedRoute.data = of({ credit });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(credit));
        expect(comp.peopleSharedCollection).toContain(person);
        expect(comp.moviesSharedCollection).toContain(movie);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const credit = { id: 123 };
        spyOn(creditService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ credit });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: credit }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(creditService.update).toHaveBeenCalledWith(credit);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const credit = new Credit();
        spyOn(creditService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ credit });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: credit }));
        saveSubject.complete();

        // THEN
        expect(creditService.create).toHaveBeenCalledWith(credit);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const credit = { id: 123 };
        spyOn(creditService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ credit });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(creditService.update).toHaveBeenCalledWith(credit);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackPersonById', () => {
        it('Should return tracked Person primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackPersonById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });

      describe('trackMovieById', () => {
        it('Should return tracked Movie primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackMovieById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });
  });
});
