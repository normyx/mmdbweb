import * as dayjs from 'dayjs';
import { IImage } from 'app/entities/mmdbmstmdb/image/image.model';

export interface IPerson {
  id?: number;
  birthday?: dayjs.Dayjs | null;
  deathday?: dayjs.Dayjs | null;
  name?: string | null;
  aka?: string | null;
  gender?: number | null;
  biography?: string | null;
  placeOfBirth?: string | null;
  homepage?: string | null;
  lastTMDBUpdate?: dayjs.Dayjs;
  tmdbId?: string;
  profiles?: IImage[] | null;
}

export class Person implements IPerson {
  constructor(
    public id?: number,
    public birthday?: dayjs.Dayjs | null,
    public deathday?: dayjs.Dayjs | null,
    public name?: string | null,
    public aka?: string | null,
    public gender?: number | null,
    public biography?: string | null,
    public placeOfBirth?: string | null,
    public homepage?: string | null,
    public lastTMDBUpdate?: dayjs.Dayjs,
    public tmdbId?: string,
    public profiles?: IImage[] | null
  ) {}
}

export function getPersonIdentifier(person: IPerson): number | undefined {
  return person.id;
}
