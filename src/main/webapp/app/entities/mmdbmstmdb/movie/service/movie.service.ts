import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMovie, getMovieIdentifier } from '../movie.model';

export type EntityResponseType = HttpResponse<IMovie>;
export type EntityArrayResponseType = HttpResponse<IMovie[]>;

@Injectable({ providedIn: 'root' })
export class MovieService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/movies', 'mmdbmstmdb');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(movie: IMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(movie);
    return this.http
      .post<IMovie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(movie: IMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(movie);
    return this.http
      .put<IMovie>(`${this.resourceUrl}/${getMovieIdentifier(movie) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(movie: IMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(movie);
    return this.http
      .patch<IMovie>(`${this.resourceUrl}/${getMovieIdentifier(movie) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMovie>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMovie[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMovieToCollectionIfMissing(movieCollection: IMovie[], ...moviesToCheck: (IMovie | null | undefined)[]): IMovie[] {
    const movies: IMovie[] = moviesToCheck.filter(isPresent);
    if (movies.length > 0) {
      const movieCollectionIdentifiers = movieCollection.map(movieItem => getMovieIdentifier(movieItem)!);
      const moviesToAdd = movies.filter(movieItem => {
        const movieIdentifier = getMovieIdentifier(movieItem);
        if (movieIdentifier == null || movieCollectionIdentifiers.includes(movieIdentifier)) {
          return false;
        }
        movieCollectionIdentifiers.push(movieIdentifier);
        return true;
      });
      return [...moviesToAdd, ...movieCollection];
    }
    return movieCollection;
  }

  protected convertDateFromClient(movie: IMovie): IMovie {
    return Object.assign({}, movie, {
      releaseDate: movie.releaseDate?.isValid() ? movie.releaseDate.format(DATE_FORMAT) : undefined,
      lastTMDBUpdate: movie.lastTMDBUpdate?.isValid() ? movie.lastTMDBUpdate.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.releaseDate = res.body.releaseDate ? dayjs(res.body.releaseDate) : undefined;
      res.body.lastTMDBUpdate = res.body.lastTMDBUpdate ? dayjs(res.body.lastTMDBUpdate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((movie: IMovie) => {
        movie.releaseDate = movie.releaseDate ? dayjs(movie.releaseDate) : undefined;
        movie.lastTMDBUpdate = movie.lastTMDBUpdate ? dayjs(movie.lastTMDBUpdate) : undefined;
      });
    }
    return res;
  }
}
