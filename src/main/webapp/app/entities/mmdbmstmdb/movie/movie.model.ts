import * as dayjs from 'dayjs';
import { ICredit } from 'app/entities/mmdbmstmdb/credit/credit.model';
import { IImage } from 'app/entities/mmdbmstmdb/image/image.model';
import { IGenre } from 'app/entities/mmdbmstmdb/genre/genre.model';
import { MovieStatus } from 'app/entities/enumerations/movie-status.model';

export interface IMovie {
  id?: number;
  title?: string | null;
  forAdult?: boolean | null;
  homepage?: string | null;
  originalLanguage?: string | null;
  originalTitle?: string | null;
  overview?: string | null;
  tagline?: string | null;
  status?: MovieStatus | null;
  voteAverage?: number | null;
  voteCount?: number | null;
  releaseDate?: dayjs.Dayjs | null;
  lastTMDBUpdate?: dayjs.Dayjs;
  tmdbId?: string;
  runtime?: number | null;
  credits?: ICredit[] | null;
  posters?: IImage[] | null;
  backdrops?: IImage[] | null;
  genres?: IGenre[] | null;
}

export class Movie implements IMovie {
  constructor(
    public id?: number,
    public title?: string | null,
    public forAdult?: boolean | null,
    public homepage?: string | null,
    public originalLanguage?: string | null,
    public originalTitle?: string | null,
    public overview?: string | null,
    public tagline?: string | null,
    public status?: MovieStatus | null,
    public voteAverage?: number | null,
    public voteCount?: number | null,
    public releaseDate?: dayjs.Dayjs | null,
    public lastTMDBUpdate?: dayjs.Dayjs,
    public tmdbId?: string,
    public runtime?: number | null,
    public credits?: ICredit[] | null,
    public posters?: IImage[] | null,
    public backdrops?: IImage[] | null,
    public genres?: IGenre[] | null
  ) {
    this.forAdult = this.forAdult ?? false;
  }
}

export function getMovieIdentifier(movie: IMovie): number | undefined {
  return movie.id;
}
