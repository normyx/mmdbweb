jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MovieService } from '../service/movie.service';
import { IMovie, Movie } from '../movie.model';
import { IGenre } from 'app/entities/mmdbmstmdb/genre/genre.model';
import { GenreService } from 'app/entities/mmdbmstmdb/genre/service/genre.service';

import { MovieUpdateComponent } from './movie-update.component';

describe('Component Tests', () => {
  describe('Movie Management Update Component', () => {
    let comp: MovieUpdateComponent;
    let fixture: ComponentFixture<MovieUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let movieService: MovieService;
    let genreService: GenreService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MovieUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(MovieUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MovieUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      movieService = TestBed.inject(MovieService);
      genreService = TestBed.inject(GenreService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should call Genre query and add missing value', () => {
        const movie: IMovie = { id: 456 };
        const genres: IGenre[] = [{ id: 31563 }];
        movie.genres = genres;

        const genreCollection: IGenre[] = [{ id: 18517 }];
        spyOn(genreService, 'query').and.returnValue(of(new HttpResponse({ body: genreCollection })));
        const additionalGenres = [...genres];
        const expectedCollection: IGenre[] = [...additionalGenres, ...genreCollection];
        spyOn(genreService, 'addGenreToCollectionIfMissing').and.returnValue(expectedCollection);

        activatedRoute.data = of({ movie });
        comp.ngOnInit();

        expect(genreService.query).toHaveBeenCalled();
        expect(genreService.addGenreToCollectionIfMissing).toHaveBeenCalledWith(genreCollection, ...additionalGenres);
        expect(comp.genresSharedCollection).toEqual(expectedCollection);
      });

      it('Should update editForm', () => {
        const movie: IMovie = { id: 456 };
        const genres: IGenre = { id: 27697 };
        movie.genres = [genres];

        activatedRoute.data = of({ movie });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(movie));
        expect(comp.genresSharedCollection).toContain(genres);
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const movie = { id: 123 };
        spyOn(movieService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ movie });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: movie }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(movieService.update).toHaveBeenCalledWith(movie);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const movie = new Movie();
        spyOn(movieService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ movie });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: movie }));
        saveSubject.complete();

        // THEN
        expect(movieService.create).toHaveBeenCalledWith(movie);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const movie = { id: 123 };
        spyOn(movieService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ movie });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(movieService.update).toHaveBeenCalledWith(movie);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });

    describe('Tracking relationships identifiers', () => {
      describe('trackGenreById', () => {
        it('Should return tracked Genre primary key', () => {
          const entity = { id: 123 };
          const trackResult = comp.trackGenreById(0, entity);
          expect(trackResult).toEqual(entity.id);
        });
      });
    });

    describe('Getting selected relationships', () => {
      describe('getSelectedGenre', () => {
        it('Should return option if no Genre is selected', () => {
          const option = { id: 123 };
          const result = comp.getSelectedGenre(option);
          expect(result === option).toEqual(true);
        });

        it('Should return selected Genre for according option', () => {
          const option = { id: 123 };
          const selected = { id: 123 };
          const selected2 = { id: 456 };
          const result = comp.getSelectedGenre(option, [selected2, selected]);
          expect(result === selected).toEqual(true);
          expect(result === selected2).toEqual(false);
          expect(result === option).toEqual(false);
        });

        it('Should return option if this Genre is not selected', () => {
          const option = { id: 123 };
          const selected = { id: 456 };
          const result = comp.getSelectedGenre(option, [selected]);
          expect(result === option).toEqual(true);
          expect(result === selected).toEqual(false);
        });
      });
    });
  });
});
