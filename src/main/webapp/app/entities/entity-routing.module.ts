import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'credit',
        data: { pageTitle: 'mmdbwebApp.mmdbmstmdbCredit.home.title' },
        loadChildren: () => import('./mmdbmstmdb/credit/credit.module').then(m => m.MmdbmstmdbCreditModule),
      },
      {
        path: 'genre',
        data: { pageTitle: 'mmdbwebApp.mmdbmstmdbGenre.home.title' },
        loadChildren: () => import('./mmdbmstmdb/genre/genre.module').then(m => m.MmdbmstmdbGenreModule),
      },
      {
        path: 'image',
        data: { pageTitle: 'mmdbwebApp.mmdbmstmdbImage.home.title' },
        loadChildren: () => import('./mmdbmstmdb/image/image.module').then(m => m.MmdbmstmdbImageModule),
      },
      {
        path: 'image-data',
        data: { pageTitle: 'mmdbwebApp.mmdbmstmdbImageData.home.title' },
        loadChildren: () => import('./mmdbmstmdb/image-data/image-data.module').then(m => m.MmdbmstmdbImageDataModule),
      },
      {
        path: 'movie',
        data: { pageTitle: 'mmdbwebApp.mmdbmstmdbMovie.home.title' },
        loadChildren: () => import('./mmdbmstmdb/movie/movie.module').then(m => m.MmdbmstmdbMovieModule),
      },
      {
        path: 'my-movie',
        data: { pageTitle: 'mmdbwebApp.mmdbmsuserMyMovie.home.title' },
        loadChildren: () => import('./mmdbmsuser/my-movie/my-movie.module').then(m => m.MmdbmsuserMyMovieModule),
      },
      {
        path: 'person',
        data: { pageTitle: 'mmdbwebApp.mmdbmstmdbPerson.home.title' },
        loadChildren: () => import('./mmdbmstmdb/person/person.module').then(m => m.MmdbmstmdbPersonModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
