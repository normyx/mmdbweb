jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { MyMovieService } from '../service/my-movie.service';
import { IMyMovie, MyMovie } from '../my-movie.model';

import { MyMovieUpdateComponent } from './my-movie-update.component';

describe('Component Tests', () => {
  describe('MyMovie Management Update Component', () => {
    let comp: MyMovieUpdateComponent;
    let fixture: ComponentFixture<MyMovieUpdateComponent>;
    let activatedRoute: ActivatedRoute;
    let myMovieService: MyMovieService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        declarations: [MyMovieUpdateComponent],
        providers: [FormBuilder, ActivatedRoute],
      })
        .overrideTemplate(MyMovieUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MyMovieUpdateComponent);
      activatedRoute = TestBed.inject(ActivatedRoute);
      myMovieService = TestBed.inject(MyMovieService);

      comp = fixture.componentInstance;
    });

    describe('ngOnInit', () => {
      it('Should update editForm', () => {
        const myMovie: IMyMovie = { id: 456 };

        activatedRoute.data = of({ myMovie });
        comp.ngOnInit();

        expect(comp.editForm.value).toEqual(expect.objectContaining(myMovie));
      });
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const myMovie = { id: 123 };
        spyOn(myMovieService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ myMovie });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: myMovie }));
        saveSubject.complete();

        // THEN
        expect(comp.previousState).toHaveBeenCalled();
        expect(myMovieService.update).toHaveBeenCalledWith(myMovie);
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', () => {
        // GIVEN
        const saveSubject = new Subject();
        const myMovie = new MyMovie();
        spyOn(myMovieService, 'create').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ myMovie });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.next(new HttpResponse({ body: myMovie }));
        saveSubject.complete();

        // THEN
        expect(myMovieService.create).toHaveBeenCalledWith(myMovie);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).toHaveBeenCalled();
      });

      it('Should set isSaving to false on error', () => {
        // GIVEN
        const saveSubject = new Subject();
        const myMovie = { id: 123 };
        spyOn(myMovieService, 'update').and.returnValue(saveSubject);
        spyOn(comp, 'previousState');
        activatedRoute.data = of({ myMovie });
        comp.ngOnInit();

        // WHEN
        comp.save();
        expect(comp.isSaving).toEqual(true);
        saveSubject.error('This is an error!');

        // THEN
        expect(myMovieService.update).toHaveBeenCalledWith(myMovie);
        expect(comp.isSaving).toEqual(false);
        expect(comp.previousState).not.toHaveBeenCalled();
      });
    });
  });
});
