import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IMyMovie, MyMovie } from '../my-movie.model';
import { MyMovieService } from '../service/my-movie.service';

@Component({
  selector: 'jhi-my-movie-update',
  templateUrl: './my-movie-update.component.html',
})
export class MyMovieUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    comments: [null, [Validators.maxLength(4000)]],
    vote: [null, [Validators.min(0), Validators.max(5)]],
    viewedDate: [],
    tmdbId: [null, [Validators.required]],
    userId: [null, [Validators.required]],
  });

  constructor(protected myMovieService: MyMovieService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ myMovie }) => {
      this.updateForm(myMovie);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const myMovie = this.createFromForm();
    if (myMovie.id !== undefined) {
      this.subscribeToSaveResponse(this.myMovieService.update(myMovie));
    } else {
      this.subscribeToSaveResponse(this.myMovieService.create(myMovie));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMyMovie>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(myMovie: IMyMovie): void {
    this.editForm.patchValue({
      id: myMovie.id,
      comments: myMovie.comments,
      vote: myMovie.vote,
      viewedDate: myMovie.viewedDate,
      tmdbId: myMovie.tmdbId,
      userId: myMovie.userId,
    });
  }

  protected createFromForm(): IMyMovie {
    return {
      ...new MyMovie(),
      id: this.editForm.get(['id'])!.value,
      comments: this.editForm.get(['comments'])!.value,
      vote: this.editForm.get(['vote'])!.value,
      viewedDate: this.editForm.get(['viewedDate'])!.value,
      tmdbId: this.editForm.get(['tmdbId'])!.value,
      userId: this.editForm.get(['userId'])!.value,
    };
  }
}
