import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMyMovie } from '../my-movie.model';
import { MyMovieService } from '../service/my-movie.service';

@Component({
  templateUrl: './my-movie-delete-dialog.component.html',
})
export class MyMovieDeleteDialogComponent {
  myMovie?: IMyMovie;

  constructor(protected myMovieService: MyMovieService, public activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.myMovieService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
