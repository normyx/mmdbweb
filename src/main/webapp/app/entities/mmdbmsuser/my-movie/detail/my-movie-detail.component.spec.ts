import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MyMovieDetailComponent } from './my-movie-detail.component';

describe('Component Tests', () => {
  describe('MyMovie Management Detail Component', () => {
    let comp: MyMovieDetailComponent;
    let fixture: ComponentFixture<MyMovieDetailComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [MyMovieDetailComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: { data: of({ myMovie: { id: 123 } }) },
          },
        ],
      })
        .overrideTemplate(MyMovieDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MyMovieDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load myMovie on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.myMovie).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
