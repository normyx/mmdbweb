import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { MyMovieComponent } from './list/my-movie.component';
import { MyMovieDetailComponent } from './detail/my-movie-detail.component';
import { MyMovieUpdateComponent } from './update/my-movie-update.component';
import { MyMovieDeleteDialogComponent } from './delete/my-movie-delete-dialog.component';
import { MyMovieRoutingModule } from './route/my-movie-routing.module';

@NgModule({
  imports: [SharedModule, MyMovieRoutingModule],
  declarations: [MyMovieComponent, MyMovieDetailComponent, MyMovieUpdateComponent, MyMovieDeleteDialogComponent],
  entryComponents: [MyMovieDeleteDialogComponent],
})
export class MmdbmsuserMyMovieModule {}
