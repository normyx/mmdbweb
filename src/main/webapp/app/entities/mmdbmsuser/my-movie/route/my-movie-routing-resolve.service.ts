import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMyMovie, MyMovie } from '../my-movie.model';
import { MyMovieService } from '../service/my-movie.service';

@Injectable({ providedIn: 'root' })
export class MyMovieRoutingResolveService implements Resolve<IMyMovie> {
  constructor(protected service: MyMovieService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMyMovie> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((myMovie: HttpResponse<MyMovie>) => {
          if (myMovie.body) {
            return of(myMovie.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MyMovie());
  }
}
