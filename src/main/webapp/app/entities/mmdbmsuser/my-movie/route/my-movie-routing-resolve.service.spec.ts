jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IMyMovie, MyMovie } from '../my-movie.model';
import { MyMovieService } from '../service/my-movie.service';

import { MyMovieRoutingResolveService } from './my-movie-routing-resolve.service';

describe('Service Tests', () => {
  describe('MyMovie routing resolve service', () => {
    let mockRouter: Router;
    let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
    let routingResolveService: MyMovieRoutingResolveService;
    let service: MyMovieService;
    let resultMyMovie: IMyMovie | undefined;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [Router, ActivatedRouteSnapshot],
      });
      mockRouter = TestBed.inject(Router);
      mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
      routingResolveService = TestBed.inject(MyMovieRoutingResolveService);
      service = TestBed.inject(MyMovieService);
      resultMyMovie = undefined;
    });

    describe('resolve', () => {
      it('should return IMyMovie returned by find', () => {
        // GIVEN
        service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMyMovie = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultMyMovie).toEqual({ id: 123 });
      });

      it('should return new IMyMovie if id is not provided', () => {
        // GIVEN
        service.find = jest.fn();
        mockActivatedRouteSnapshot.params = {};

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMyMovie = result;
        });

        // THEN
        expect(service.find).not.toBeCalled();
        expect(resultMyMovie).toEqual(new MyMovie());
      });

      it('should route to 404 page if data not found in server', () => {
        // GIVEN
        spyOn(service, 'find').and.returnValue(of(new HttpResponse({ body: null })));
        mockActivatedRouteSnapshot.params = { id: 123 };

        // WHEN
        routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
          resultMyMovie = result;
        });

        // THEN
        expect(service.find).toBeCalledWith(123);
        expect(resultMyMovie).toEqual(undefined);
        expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
      });
    });
  });
});
