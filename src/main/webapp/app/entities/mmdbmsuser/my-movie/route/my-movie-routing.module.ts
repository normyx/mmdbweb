import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MyMovieComponent } from '../list/my-movie.component';
import { MyMovieDetailComponent } from '../detail/my-movie-detail.component';
import { MyMovieUpdateComponent } from '../update/my-movie-update.component';
import { MyMovieRoutingResolveService } from './my-movie-routing-resolve.service';

const myMovieRoute: Routes = [
  {
    path: '',
    component: MyMovieComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MyMovieDetailComponent,
    resolve: {
      myMovie: MyMovieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MyMovieUpdateComponent,
    resolve: {
      myMovie: MyMovieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MyMovieUpdateComponent,
    resolve: {
      myMovie: MyMovieRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(myMovieRoute)],
  exports: [RouterModule],
})
export class MyMovieRoutingModule {}
