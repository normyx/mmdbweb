import { Component, OnInit } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMyMovie } from '../my-movie.model';

import { ITEMS_PER_PAGE } from 'app/config/pagination.constants';
import { MyMovieService } from '../service/my-movie.service';
import { MyMovieDeleteDialogComponent } from '../delete/my-movie-delete-dialog.component';
import { ParseLinks } from 'app/core/util/parse-links.service';

@Component({
  selector: 'jhi-my-movie',
  templateUrl: './my-movie.component.html',
})
export class MyMovieComponent implements OnInit {
  myMovies: IMyMovie[];
  isLoading = false;
  itemsPerPage: number;
  links: { [key: string]: number };
  page: number;
  predicate: string;
  ascending: boolean;

  constructor(protected myMovieService: MyMovieService, protected modalService: NgbModal, protected parseLinks: ParseLinks) {
    this.myMovies = [];
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.page = 0;
    this.links = {
      last: 0,
    };
    this.predicate = 'id';
    this.ascending = true;
  }

  loadAll(): void {
    this.isLoading = true;

    this.myMovieService
      .query({
        page: this.page,
        size: this.itemsPerPage,
        sort: this.sort(),
      })
      .subscribe(
        (res: HttpResponse<IMyMovie[]>) => {
          this.isLoading = false;
          this.paginateMyMovies(res.body, res.headers);
        },
        () => {
          this.isLoading = false;
        }
      );
  }

  reset(): void {
    this.page = 0;
    this.myMovies = [];
    this.loadAll();
  }

  loadPage(page: number): void {
    this.page = page;
    this.loadAll();
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMyMovie): number {
    return item.id!;
  }

  delete(myMovie: IMyMovie): void {
    const modalRef = this.modalService.open(MyMovieDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.myMovie = myMovie;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.reset();
      }
    });
  }

  protected sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateMyMovies(data: IMyMovie[] | null, headers: HttpHeaders): void {
    this.links = this.parseLinks.parse(headers.get('link') ?? '');
    if (data) {
      for (const d of data) {
        this.myMovies.push(d);
      }
    }
  }
}
