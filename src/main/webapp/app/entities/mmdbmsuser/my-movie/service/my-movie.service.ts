import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMyMovie, getMyMovieIdentifier } from '../my-movie.model';

export type EntityResponseType = HttpResponse<IMyMovie>;
export type EntityArrayResponseType = HttpResponse<IMyMovie[]>;

@Injectable({ providedIn: 'root' })
export class MyMovieService {
  public resourceUrl = this.applicationConfigService.getEndpointFor('api/my-movies', 'mmdbmsuser');

  constructor(protected http: HttpClient, private applicationConfigService: ApplicationConfigService) {}

  create(myMovie: IMyMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(myMovie);
    return this.http
      .post<IMyMovie>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(myMovie: IMyMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(myMovie);
    return this.http
      .put<IMyMovie>(`${this.resourceUrl}/${getMyMovieIdentifier(myMovie) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(myMovie: IMyMovie): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(myMovie);
    return this.http
      .patch<IMyMovie>(`${this.resourceUrl}/${getMyMovieIdentifier(myMovie) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMyMovie>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMyMovie[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMyMovieToCollectionIfMissing(myMovieCollection: IMyMovie[], ...myMoviesToCheck: (IMyMovie | null | undefined)[]): IMyMovie[] {
    const myMovies: IMyMovie[] = myMoviesToCheck.filter(isPresent);
    if (myMovies.length > 0) {
      const myMovieCollectionIdentifiers = myMovieCollection.map(myMovieItem => getMyMovieIdentifier(myMovieItem)!);
      const myMoviesToAdd = myMovies.filter(myMovieItem => {
        const myMovieIdentifier = getMyMovieIdentifier(myMovieItem);
        if (myMovieIdentifier == null || myMovieCollectionIdentifiers.includes(myMovieIdentifier)) {
          return false;
        }
        myMovieCollectionIdentifiers.push(myMovieIdentifier);
        return true;
      });
      return [...myMoviesToAdd, ...myMovieCollection];
    }
    return myMovieCollection;
  }

  protected convertDateFromClient(myMovie: IMyMovie): IMyMovie {
    return Object.assign({}, myMovie, {
      viewedDate: myMovie.viewedDate?.isValid() ? myMovie.viewedDate.format(DATE_FORMAT) : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.viewedDate = res.body.viewedDate ? dayjs(res.body.viewedDate) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((myMovie: IMyMovie) => {
        myMovie.viewedDate = myMovie.viewedDate ? dayjs(myMovie.viewedDate) : undefined;
      });
    }
    return res;
  }
}
