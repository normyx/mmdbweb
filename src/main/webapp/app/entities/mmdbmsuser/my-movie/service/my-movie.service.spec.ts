import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_FORMAT } from 'app/config/input.constants';
import { IMyMovie, MyMovie } from '../my-movie.model';

import { MyMovieService } from './my-movie.service';

describe('Service Tests', () => {
  describe('MyMovie Service', () => {
    let service: MyMovieService;
    let httpMock: HttpTestingController;
    let elemDefault: IMyMovie;
    let expectedResult: IMyMovie | IMyMovie[] | boolean | null;
    let currentDate: dayjs.Dayjs;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      expectedResult = null;
      service = TestBed.inject(MyMovieService);
      httpMock = TestBed.inject(HttpTestingController);
      currentDate = dayjs();

      elemDefault = {
        id: 0,
        comments: 'AAAAAAA',
        vote: 0,
        viewedDate: currentDate,
        tmdbId: 'AAAAAAA',
        userId: 0,
      };
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            viewedDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a MyMovie', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            viewedDate: currentDate.format(DATE_FORMAT),
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            viewedDate: currentDate,
          },
          returnedFromService
        );

        service.create(new MyMovie()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a MyMovie', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            comments: 'BBBBBB',
            vote: 1,
            viewedDate: currentDate.format(DATE_FORMAT),
            tmdbId: 'BBBBBB',
            userId: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            viewedDate: currentDate,
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should partial update a MyMovie', () => {
        const patchObject = Object.assign(
          {
            viewedDate: currentDate.format(DATE_FORMAT),
            tmdbId: 'BBBBBB',
            userId: 1,
          },
          new MyMovie()
        );

        const returnedFromService = Object.assign(patchObject, elemDefault);

        const expected = Object.assign(
          {
            viewedDate: currentDate,
          },
          returnedFromService
        );

        service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PATCH' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of MyMovie', () => {
        const returnedFromService = Object.assign(
          {
            id: 1,
            comments: 'BBBBBB',
            vote: 1,
            viewedDate: currentDate.format(DATE_FORMAT),
            tmdbId: 'BBBBBB',
            userId: 1,
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            viewedDate: currentDate,
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a MyMovie', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });

      describe('addMyMovieToCollectionIfMissing', () => {
        it('should add a MyMovie to an empty array', () => {
          const myMovie: IMyMovie = { id: 123 };
          expectedResult = service.addMyMovieToCollectionIfMissing([], myMovie);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(myMovie);
        });

        it('should not add a MyMovie to an array that contains it', () => {
          const myMovie: IMyMovie = { id: 123 };
          const myMovieCollection: IMyMovie[] = [
            {
              ...myMovie,
            },
            { id: 456 },
          ];
          expectedResult = service.addMyMovieToCollectionIfMissing(myMovieCollection, myMovie);
          expect(expectedResult).toHaveLength(2);
        });

        it("should add a MyMovie to an array that doesn't contain it", () => {
          const myMovie: IMyMovie = { id: 123 };
          const myMovieCollection: IMyMovie[] = [{ id: 456 }];
          expectedResult = service.addMyMovieToCollectionIfMissing(myMovieCollection, myMovie);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(myMovie);
        });

        it('should add only unique MyMovie to an array', () => {
          const myMovieArray: IMyMovie[] = [{ id: 123 }, { id: 456 }, { id: 42770 }];
          const myMovieCollection: IMyMovie[] = [{ id: 123 }];
          expectedResult = service.addMyMovieToCollectionIfMissing(myMovieCollection, ...myMovieArray);
          expect(expectedResult).toHaveLength(3);
        });

        it('should accept varargs', () => {
          const myMovie: IMyMovie = { id: 123 };
          const myMovie2: IMyMovie = { id: 456 };
          expectedResult = service.addMyMovieToCollectionIfMissing([], myMovie, myMovie2);
          expect(expectedResult).toHaveLength(2);
          expect(expectedResult).toContain(myMovie);
          expect(expectedResult).toContain(myMovie2);
        });

        it('should accept null and undefined values', () => {
          const myMovie: IMyMovie = { id: 123 };
          expectedResult = service.addMyMovieToCollectionIfMissing([], null, myMovie, undefined);
          expect(expectedResult).toHaveLength(1);
          expect(expectedResult).toContain(myMovie);
        });
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
