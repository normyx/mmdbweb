import * as dayjs from 'dayjs';

export interface IMyMovie {
  id?: number;
  comments?: string | null;
  vote?: number | null;
  viewedDate?: dayjs.Dayjs | null;
  tmdbId?: string;
  userId?: number;
}

export class MyMovie implements IMyMovie {
  constructor(
    public id?: number,
    public comments?: string | null,
    public vote?: number | null,
    public viewedDate?: dayjs.Dayjs | null,
    public tmdbId?: string,
    public userId?: number
  ) {}
}

export function getMyMovieIdentifier(myMovie: IMyMovie): number | undefined {
  return myMovie.id;
}
