/**
 * View Models used by Spring MVC REST controllers.
 */
package com.mgoulene.mmdb.web.rest.vm;
